# Config

1. Go to **config.js** file and change the config (just need to change the gdrive config)


```
gdrive: {
	scopes: ['https://www.googleapis.com/auth/spreadsheets'],	token_path: 'token.json',
	credentials_path: 'credentials.json',
	spreadsheetId: '122kOIeW6C6qLySRr8OJb71bMrn-5b2HjPTzIoDoeV-I'
}
```


- **scopes, token_path**: no need to change

- **credentials_path**: no need to change but need get credentials.json file from Google

	- go to https://developers.google.com/sheets/api/quickstart/nodejs
	- click button Enable the google sheets API, it will open a popup and you 		wait a bit
	- then click button "Download client configuration" on the popup, it will 	download file "credentials.json", then put it in this folder (same level 	with config.js)
	
- **spreadsheetId**: 
	- The id of your spreadsheet
	- example your link is https://docs.google.com/spreadsheets/d/122kOIeW6C6qLySRr8OJb71bMrn-5b2HjPTzIoDoeV-I/edit, so the Id is "122kOIeW6C6qLySRr8OJb71bMrn-5b2HjPTzIoDoeV-I"
	
	
2. In your PC (recommend MAC or Linux OS), need install:
 - nodejs v10 https://nodejs.org/en/download/

3. Open terminal, go to this folder, ( cd /go/to/this/folder), then run following command
 - npm install
 - node index.js -> to scraping directly next-engine and racoon website (1)
 - DEBUG=task-manager-ui:* npm start -> to run UI, then go to http://localhost:3000 to see the UI (2)

 Just need run only (1) or (2), recommend run (2)
