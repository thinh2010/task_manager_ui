const AbstractScraping = require('./AbstractScraping')
const config = require('./../../config')
const report = require('./../../report-next-engine')

class NextEngine extends AbstractScraping {

	async login() {
        try {
            await this.page.goto(config.nextEngineUrl)
            await this.page.type('#user_login_code', config.nextEngineUser)
            await this.page.type('#user_password', config.nextEnginePass)

            await this.page.click('.btn-primary')
            await this.page.waitForSelector('.user_nav')
        } catch(e) {
            console.log('login error: ', e)
        }
        
        return await this.page.$('.user_nav') !== null
    }

    async getReport() {
        try {
            this.page.on('dialog', async dialog => {
                await dialog.dismiss();
            });
            await this.page.click('.user_nav li:first-child')
            await this.page.click('.user_nav .home-setting-menu a')
            await this.page.waitFor(5000)
            await this.page.goto(config.nextEngineReportUrl)
            await this.page.click('#jyuchu_dlg_open')
            await this.page.select('#xxx_jyuchu_search_field00', 'S_ALL')
            await this.page.click('#ne_dlg_btn2_searchJyuchuDlg')
            await this.page.waitFor(2000) // wait for turn off alert
            await this.page.click('#searchJyuchu_table_dl_lnk')

            return true
        } catch(e) {
            console.log('get report error: ', e)
            return false
        }
    }

    async writeReport() {
        await this.page.waitFor(10000)
        await report()
    }

}

module.exports = NextEngine