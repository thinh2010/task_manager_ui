const AbstractScraping = require('./AbstractScraping')
const config = require('./../../config')
const report = require('./../../report-racoon')


class Racoon extends AbstractScraping {

	async login() {
        try {
            await this.page.goto(config.racoonUrl)
            await this.page.type('#username', config.racoonUser)
            await this.page.type('#password', config.racoonPass)
            await this.page.click('#login-submit')
            await this.page.waitForSelector('#loggedas')
        } catch(e) {
            console.log('errr', e)
        }
        
        console.log('wait')

        return await this.page.$('#loggedas') !== null
    }

    async getReport() {
        await this.page.goto(config.racoonReportUrl)
        const needle = 'レポート'
        const baseUrl = 'https://inquiry.logistics.rakuten.co.jp'
        const link = await this.page.evaluate(needle => {
            return $(`td:contains(${needle})`).first().parent().find('td.id a').attr('href')

        }, needle)

        if (link == undefined) {
            return false
        }
        
        const reportLink = baseUrl + link
        await this.page.goto(reportLink)
        await this.page.click('a.icon-download')

        return true
        
        // await this.page.click('#jyuchu_dlg_open')
        // await this.page.select('#xxx_jyuchu_search_field00', 'S_ALL')
        // await this.page.click('#ne_dlg_btn2_searchJyuchuDlg')
        // await this.page.waitFor(2000) // wait for turn off alert
        // await this.page.click('#searchJyuchu_table_dl_lnk')
    }

    async writeReport() {
        await this.page.waitFor(10000) // wait 10s to download report
        await report()
    }
}

module.exports = Racoon