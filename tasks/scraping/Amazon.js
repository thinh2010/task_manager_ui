const AbstractScraping = require('./AbstractScraping')
const config = require('./../../config')
const fs = require('fs')
const report = require('./../../report-amazon')
const report2 = require('./../../report-amazon2')


class Amazon extends AbstractScraping {

	async login() {
        try {
            console.log('etesstet')
            await this.page.goto(config.amzUrl)
            await this.page.type('#ap_email', config.amzUser)
            await this.page.type('#ap_password', config.amzPass)
            console.log(config.amzUrl, config.amzUser, config.amzPass)
            await this.page.click('#signInSubmit')
            // await this.page.waitFor(30000)
            await this.page.waitForSelector('.sc-quick-txt', {timeout: 30000})
            // const elm = await this.page.$('#auth-mfa-otpcode');
            // console.log(elm)

            // if (await this.page.$('#auth-mfa-otpcode') !== null) {
            //     // require OTP
            //     console.log('need optcode')
            //     const content = await this.watchFile()
            //     console.log(content)
            // }
            // await this.page.waitForSelector('#loggedas')
        } catch(e) {
            console.log('errr', e)
        }
        
        console.log('wait')

        return await this.page.$('.sc-quick-txt') !== null
        // return true
    }

    async getReport() {
        await this.getReport2()

        return true
        await this.page.goto(config.amzReportUrl)
        const needle = 'FBA在庫レポート'
        const baseUrl = 'https://sellercentral-japan.amazon.com'
        await this.page.evaluate(needle => {
            $(`td:contains(${needle})`).first().parent().find('td.mt-cell div.mt-link a').get(0).click()
        }, needle)

        // const reportLink = await this.page.$()
        // await reportLink.click()

        // if (link == undefined) {
        //     return false
        // }
        
        // const reportLink = baseUrl + link
        // await this.page.goto(reportLink)
        await this.page.waitFor(3000)

        
        // await this.page.click('a.icon-download')
        
        // await this.page.click('#jyuchu_dlg_open')
        // await this.page.select('#xxx_jyuchu_search_field00', 'S_ALL')
        // await this.page.click('#ne_dlg_btn2_searchJyuchuDlg')
        // await this.page.waitFor(2000) // wait for turn off alert
        // await this.page.click('#searchJyuchu_table_dl_lnk')
    }

    async getReport2() {
        await this.page.goto(config.amzReportUrl2)
        await this.page.waitForSelector('#timeFrame')
        await this.page.click('#report_DetailSalesTrafficByChildItem')
        await this.page.waitFor(10000)
        // await this.page.waitForSelector('._AR_sc_mat-ss_reports_title')
        await this.page.click('#export')
        await this.page.waitFor(2000)
        await this.page.click('#downloadCSV')

        await this.page.waitFor(3000)
        report2()
        await this.page.waitFor(10000)
        console.log('okokok report2')
        
    }

    async watchFile() {
        return await new Promise((resolve, reject) => {
            fs.watchFile('./code.txt', async (curr, prev) => {
                try {
                    const content = await fs.readFile('./code.txt')
                    console.log('readfile', content)
                    resolve(content)
                } catch(err) {
                    reject(err)
                }
            })
        }) 
    }

    async writeReport() {
        report()
    }
}

module.exports = Amazon