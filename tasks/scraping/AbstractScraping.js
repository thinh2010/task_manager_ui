const clone                     = require('clone');
const winston = require('winston');
const puppeteer = require('puppeteer');


class AbstractScraping {

    constructor(payload) {
        this.browser          = undefined;
        this.params           = clone(payload)

        const logger = winston.createLogger({
          transports: [
            // new winston.transports.Console(),
            new winston.transports.File({ filename: ROOT_PATH + '/logs/combined.log' })
          ]
        });

        this.logger = logger
        this.downloadPath = ROOT_PATH + '/downloads/' + this.params.name
    }

    /**
     * Init data
     */
    async init() {
        if (this.browser === undefined) {
            this.browser = await puppeteer.launch({ headless: this.params.headless, timeout: 0 });
        }

        this.page = await this.browser.newPage();
        this.page.on('console', (...args) => this.logger.debug('PAGE LOG:', ...args));
        await this.page.setViewport({ width: 1920, height: 926 });

        // this.nextEngineDownloadPath = './downloads/next-enginee/';

        await this.page._client.send('Page.setDownloadBehavior', {
            behavior: 'allow',
            downloadPath: this.downloadPath,
        });
        console.log('finished init')
    }

    async run() {
        console.log(this.params.name, 'start')
        console.log('='.repeat(50))
        await this.init()
        // this.logger.error('test error')
        console.log('Go to login')
        const canLogin = await this.login()
        if (!canLogin) {
            console.log('Login fail')
            await this.closeBrowser()
            return false
        }
        console.log('getReport')
        const hasReport = await this.getReport()
        if (!hasReport) {
            console.log('GetReport fail')
            await this.closeBrowser()
            return false
        }
        console.log('getReport done')

        console.log('write report')
        await this.writeReport()
        console.log('write report done')

        await this.closeBrowser()
        
        return true
    }

    /**
     * Login
     * @returns {Promise.<void>}
     */
    async login() {
        throw new Error('Need implement login method');
    }

    // /**
    //  * Execute
    //  * @returns {Promise.<void>}
    //  */
    // async execute() {
    //     throw new Error('Need implement execute method');
    // }

    /**
     * getReport
     * @returns {Promise.<void>}
     */
    async getReport() {
        throw new Error('Need implement getReport method');
    }

    async closeBrowser() {
        this.browser !== undefined && await this.browser.close();
    }
}

module.exports = AbstractScraping