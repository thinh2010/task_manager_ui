const path = require('path');
const config = require('./config')
const fs = require('fs');
const lineReader = require('line-reader');
const {google} = require('googleapis');
const { promisify } = require('util');
const GoogleDrive = require('./gdrive')
var iconv = require('iconv-lite');

const readdir = promisify(fs.readdir);

const wrireFile = async function(auth) {
    const dir = ROOT_PATH + '/downloads/amz/'
    const sheets = google.sheets({version: 'v4', auth});

    try {
        const files = await readdir(dir);

        let csvFile = ''
        //listing all files using forEach
        files.forEach(function (file) {
            csvFile = file 
            
        });

        let inputValues = []

        // Convert encoding streaming example
        // const outputFile = dir + 'data.csv'
        // console.log(outputFile)
        // await convertEncodingFile(dir + csvFile, outputFile)

        // fs.createReadStream(dir + csv)
        //     .pipe(iconv.decodeStream('shift_jis'))
        //     .pipe(iconv.encodeStream('UTF-8'))
        //     .pipe(fs.createWriteStream(outputFile));

        // fs.createReadStream(outputFile)
        //   .pipe(csv())
        //   .on('data', (row) => {
        //     console.log(row);
        //   })
        //   .on('end', () => {
        //     console.log('CSV file successfully processed');
        //   });

        console.log(csvFile)
        sellerSkuDelete = []
        sellerSkuDelete.push('sw-l-1-pink')
        sellerSkuDelete.push('sw-m-2-xxo-white')
        sellerSkuDelete.push('sw-m-2-xxxo-white')
        lineReader.eachLine(dir + csvFile , function (line, last) {
            // console.log(line.split(','))
            
            // let str = line.replace(/"/g,"")
            // console.log(str)
            let arr = line.split('\t')
            if (sellerSkuDelete.indexOf(arr[0].toLowerCase()) != -1) {
                arr[0] = ''
                arr[1] = ''
                arr[2] = ''
            }
            if (arr[4] != 'UNSELLABLE') {
                inputValues.push(arr)
            }
            // console.log(last)
            if (last) {
                // console.log(inputValues)
                return false
            }
        })

        console.log('clear first')

        console.log(inputValues)

        await clearSheet(sheets)

        console.log('write new')

        sheets.spreadsheets.values.append({
          spreadsheetId: config.gdrive.spreadsheetId,
          range: 'FBA!A1',
          valueInputOption: 'USER_ENTERED',
          resource: {values: inputValues, majorDimension: "ROWS"},
        }, (err, result) => {
          if (err) {
            // Handle error
            console.log(err);
          } else {
            console.log('Speadsheets updated!!!');
          }
        });

        // remove downloaded file
        try {
          fs.unlinkSync(dir + csvFile)
          //file removed
        } catch(err) {
          console.error(err)
        }
            
    } catch (err) {
        return console.log('Unable to scan directory:', err);
    } 

    // await fs.readdir(dir, function (err, files) {
    //     //handling error
    //     if (err) {
    //         return console.log('Unable to scan directory: ' + err);
    //     } 
        
    // });
}

async function clearSheet(sheets) {
    return new Promise((resolve, reject) => {
        sheets.spreadsheets.values.clear({
          spreadsheetId: config.gdrive.spreadsheetId,
          range: 'FBA!A:F',
          // valueInputOption: 'USER_ENTERED',
          // resource: {values: inputValues, majorDimension: "ROWS"},
        }, (err, result) => {
          if (err) {
            // Handle error
            reject(err);
          } else {
            console.log('Cleared.');
            resolve('ok')
          }
        });
    })
}

function convertEncodingFile(input, output) {
    return new Promise(resolve => {
        let file = fs.createWriteStream(output);
        fs.createReadStream(input)
        .pipe(iconv.decodeStream('shift_jis'))
        .pipe(iconv.encodeStream('UTF-8'))
        .pipe(file);

        file.on('finish', resolve)
    })
}

const execute = () => {
    console.log('execute')
    const gdrive = new GoogleDrive(
        config.gdrive.credentials_path, 
        config.gdrive.token_path, 
        config.gdrive.scopes)

    gdrive.run(wrireFile)
}

module.exports = execute

