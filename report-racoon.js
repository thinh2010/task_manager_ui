const path = require('path');
const config = require('./config')
const fs = require('fs');
const lineReader = require('line-reader');
const {google} = require('googleapis');
const { promisify } = require('util');
const GoogleDrive = require('./gdrive')
var iconv = require('iconv-lite');
var unzip = require('unzip');

const readdir = promisify(fs.readdir);

const wrireFile = async function(auth) {
    const dir = ROOT_PATH + '/downloads/racoon/'
    const extractDir = ROOT_PATH + '/downloads/extract/'
    const sheets = google.sheets({version: 'v4', auth});

    try {
        const files = await readdir(dir);

        let zipFile = ''
        //listing all files using forEach
        files.forEach(function (file) {
            zipFile = file 
            
        });

        console.log(dir + zipFile)

        // await extractZipFile(dir + zipFile, extractDir)
        fs.createReadStream(dir + zipFile).pipe(unzip.Extract({ path: extractDir }))

        await wait(3000)

        console.log('read extract dir')

        const csvFiles = await readdir(extractDir);

        const listFile = []

        csvFiles.forEach(function (file) {
            listFile.push(file) 
            
        });

        console.log('list file')

        csvFile = listFile[0]

        let inputValues = []

        // Convert encoding streaming example
        const outputFile = extractDir + 'data.csv'
        console.log(extractDir + csvFile)
        console.log(outputFile)
        await convertEncodingFile(extractDir + csvFile, outputFile)

        lineReader.eachLine(outputFile, function (line, last) {
            // console.log(line.split(','))
            
            let str = line.replace(/"/g,"")
            // console.log(str)
            inputValues.push(str.split(','))
            // console.log(last)
            if (last) {
                // console.log(inputValues)
                return false
            }
        })

        console.log('clear first')

        await clearSheet(sheets)

        console.log('write new')

        sheets.spreadsheets.values.append({
          spreadsheetId: config.gdrive.spreadsheetId,
          range: 'RSL!A1',
          valueInputOption: 'USER_ENTERED',
          resource: {values: inputValues, majorDimension: "ROWS"},
        }, (err, result) => {
          if (err) {
            // Handle error
            console.log(err);
          } else {
            console.log('Speadsheets updated!!!');
          }
        });

        csvFiles.forEach(function (file) {
            try {
              fs.unlinkSync(extractDir + file)
              //file removed
            } catch(err) {
              console.error(err)
            }
            
        });

        try {
          fs.unlinkSync(dir + zipFile)
          //file removed
        } catch(err) {
          console.error(err)
        }

        try {
          fs.unlinkSync(outputFile)
          //file removed
        } catch(err) {
          console.error(err)
        }

        
    } catch (err) {
        return console.log('Unable to scan directory:', err);
    } 
}

async function wait(sec) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve('ok')
        }, sec)
    })
}

async function extractZipFile(zip, path) {
    return new Promise((resolve, reject) => {
        
        console.log('extracting')
        resolve(fs.createReadStream(zip).pipe(unzip.Extract({ path: path })))
    })
}
async function clearSheet(sheets) {
    return new Promise((resolve, reject) => {
        sheets.spreadsheets.values.clear({
          spreadsheetId: config.gdrive.spreadsheetId,
          range: 'RSL',
        }, (err, result) => {
          if (err) {
            // Handle error
            reject(err);
          } else {
            console.log('Cleared.');
            resolve('ok')
          }
        });
    })
}

function convertEncodingFile(input, output) {
    return new Promise(resolve => {
        let file = fs.createWriteStream(output);
        fs.createReadStream(input)
        .pipe(iconv.decodeStream('shift_jis'))
        .pipe(iconv.encodeStream('UTF-8'))
        .pipe(file);

        file.on('finish', resolve)
    })
}

const execute = () => {
    const gdrive = new GoogleDrive(
        config.gdrive.credentials_path, 
        config.gdrive.token_path, 
        config.gdrive.scopes)

    gdrive.run(wrireFile)
}

module.exports = execute

