const path = require('path');
const config = require('./config')
const fs = require('fs');
const lineReader = require('line-reader');
const {google} = require('googleapis');
const { promisify } = require('util');
const GoogleDrive = require('./gdrive')
var iconv = require('iconv-lite');

const readdir = promisify(fs.readdir);

const wrireFile = async function(auth) {
    const dir = ROOT_PATH + '/downloads/amz/'
    const sheets = google.sheets({version: 'v4', auth});

    try {
        const files = await readdir(dir);

        let csvFile = ''
        //listing all files using forEach
        files.forEach(function (file) {
            csvFile = file 
            
        });

        let inputValues = []

        console.log(csvFile)
        lineReader.eachLine(dir + csvFile , function (line, last) {
            line = line.substr(1)
            line = line.slice(0, -1)
            let arr = line.split('","')
            inputValues.push(arr)

            if (last) {
                // console.log(inputValues)
                return false
            }
        })

        console.log('clear first')

        await clearSheet(sheets)

        console.log('write new')

        sheets.spreadsheets.values.append({
          spreadsheetId: config.gdrive.spreadsheetId,
          range: 'AMA!A1',
          valueInputOption: 'USER_ENTERED',
          resource: {values: inputValues, majorDimension: "ROWS"},
        }, (err, result) => {
          if (err) {
            // Handle error
            console.log(err);
          } else {
            console.log('Speadsheets updated!!!');
          }
        });

        // remove downloaded file
        try {
          fs.unlinkSync(dir + csvFile)
          //file removed
          console.log('removed report 2')
        } catch(err) {
          console.error(err)
        }
            
    } catch (err) {
        return console.log('Unable to scan directory:', err);
    } 

    // await fs.readdir(dir, function (err, files) {
    //     //handling error
    //     if (err) {
    //         return console.log('Unable to scan directory: ' + err);
    //     } 
        
    // });
}

async function clearSheet(sheets) {
    return new Promise((resolve, reject) => {
        sheets.spreadsheets.values.clear({
          spreadsheetId: config.gdrive.spreadsheetId,
          range: 'AMA',
          // valueInputOption: 'USER_ENTERED',
          // resource: {values: inputValues, majorDimension: "ROWS"},
        }, (err, result) => {
          if (err) {
            // Handle error
            reject(err);
          } else {
            console.log('Cleared.');
            resolve('ok')
          }
        });
    })
}

function convertEncodingFile(input, output) {
    return new Promise(resolve => {
        let file = fs.createWriteStream(output);
        fs.createReadStream(input)
        .pipe(iconv.decodeStream('shift_jis'))
        .pipe(iconv.encodeStream('UTF-8'))
        .pipe(file);

        file.on('finish', resolve)
    })
}

const execute = () => {
    console.log('execute')
    const gdrive = new GoogleDrive(
        config.gdrive.credentials_path, 
        config.gdrive.token_path, 
        config.gdrive.scopes)

    gdrive.run(wrireFile)
}

module.exports = execute

