var express = require('express');
var router = express.Router();
const task = require('./../tasks/task1')

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Scraping Report Tool' });
});

router.get('/task1', async (req, res, next) => {
	await task()
})

module.exports = router;
