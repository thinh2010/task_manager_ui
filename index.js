global.ROOT_PATH = __dirname

const NextEngine = require('./tasks/scraping/NextEngine')
const Racoon = require('./tasks/scraping/Racoon')

execute = async () => {
	const task2 = new Racoon({name: 'racoon'})
	await task2.run()

	const task = new NextEngine({name: 'next-engine'})
	await task.run()
} 

execute()